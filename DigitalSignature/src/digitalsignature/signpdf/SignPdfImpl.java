package digitalsignature.signpdf;

import java.io.File;
import java.io.FileOutputStream;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfSignatureAppearance.RenderingMode;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClient;

import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
/**
 * 
 * @author Manjunath
 *
 */
public class SignPdfImpl implements SignPdf {
	private static final Logger log= Logger.getLogger(SignPdfImpl.class);
	public String sign(String src, String dest, Certificate[] chain, PrivateKey pk, String digestAlgorithm,
			String provider, CryptoStandard subfilter, String reason, String location, Collection<CrlClient> crlList,
			OcspClient ocspClient, TSAClient tsaClient, int estimatedSize)
					throws GeneralSecurityException, IOException, DocumentException {
		log.info(" SignPdfImpl -> Inside sign()");
		PDDocument doc = PDDocument.load(new File(src));
		int count = doc.getNumberOfPages();
		System.out.println(" count of pages "+count);
		// Creating the reader and the stamper
		PdfReader reader = new PdfReader(src);
		FileOutputStream os = new FileOutputStream(dest);
		PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
		log.info(" SignPdfImpl -> Sign() ->Created the reader and the stamper");
		/**
		 * Modify the logic by bhagya on Feb 01st, 2021 as per client requirement
		 * adding the digital signature to the last page of document
		 * and adding the " continue..." text at remaining pages of document
		 */
		for(int i=1;i<=count;i++) {
			
		if(i!=count) {
			PdfContentByte content = stamper.getOverContent(i);
			// add text
			ColumnText ct = new ColumnText( content );
			// this are the coordinates where you want to add text
			// if the text does not fit inside it will be cropped
			ct.setSimpleColumn(560, 20,390, 70);
			//ct.setText(new Phrase(" Continue ..."));
                        ct.setText(new Phrase(" Page "+i+" of "+count));
			ct.go();
		}
		// Creating the appearance
		else {
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		appearance.setReason("I approved this document");
		appearance.setLocation("Bangalore");
		appearance.setRenderingMode(RenderingMode.DESCRIPTION);
		appearance.setAcro6Layers(false); // to show the green tick mark
		
		appearance.setVisibleSignature(new Rectangle(560, 20,390, 70), count, "SignHere"); 
		
		log.info(" SignPdfImpl -> Sign() ->Created the appearance");
		// Creating the signature
		ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,subfilter);
		
		
		}
		
		}
		stamper.close();
		os.flush();
		os.close();
		reader.close();
                doc.close();
		log.info(" SignPdfImpl -> Sign() ->Created the signature");
		return "success";

	}


}
