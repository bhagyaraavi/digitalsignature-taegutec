package digitalsignature.fileupload;
import javax.swing.JFileChooser;
import java.io.*;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JOptionPane;
import digitalsignature.service.ServiceClass;

import digitalsignature.signpdf.SignPdf;
import digitalsignature.signpdf.SignPdfImpl;
import digitalsignature.writetext.WriteTextDao;
import digitalsignature.writetext.WriteTextDaoImpl;

import sun.security.pkcs11.SunPKCS11;

import java.awt.Font;
import java.awt.Window.Type;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Toolkit;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.CrlClientOnline;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import digitalsignature.service.MonitorDirectory;
import java.security.Provider;
/**
 * 
 * @author Manjunath Service method to add text, signature and merging the files
 *
 */
public class FileUploadService {
	private static final Logger log= Logger.getLogger(FileUploadService.class);
	public String SRC = null;
	public String DEST = null;
	// hardcoded the  USB digital device password because the client is going to use one device and system should need to run this process by 24/7 with out user login 
	public static String password =null; 
	WriteTextDao writeTextDao = new WriteTextDaoImpl();
	SignPdf signPdf = new SignPdfImpl();
	ServiceClass serviceClass = new ServiceClass();
	String processFileName = null;
	MonitorDirectory monitorDirectory = new MonitorDirectory();
	String res = null;
	/**
	 * Created BY Bhagya on Sep 29th, 2020
	 * Service for to create the digital signature to invoices
	 */
	public void createSignatureToInvoice(File[] invoicePDFFiles,String folderName,String invoiceFolderPath) {
		
		
		try {
			
				for (File fl : invoicePDFFiles) {
                //allowing only pdf files for signature process 
                if (fl.getName().toLowerCase().endsWith(".pdf")) {
                    processFileName = fl.getName();
                    log.info(" FileUploadService -> File NAme -> " + processFileName);
                    String Src = fl.getAbsolutePath();
                    try {
                        // adding digital signature to final pdf and saving it in signedpdf folder
                        SRC = Src;
                        File finalDir = new File("D:\\KNS Digital Signature\\SIGNED INVOICES\\");
                        if (!finalDir.exists()) {
                            finalDir.mkdir();
                        }
                        DEST = finalDir + "\\" + processFileName;
                        log.info(" FileUploadService -> Destination File location -> " + DEST);
                        String pin = password;
                        //JAVA 9
                       // Provider prototype = Security.getProvider("SunPKCS11");
                        //Provider providerPKCS11 = prototype.configure("D:\\KNS Digital Signature\\processfile\\cfgfile\\pkcs11.cfg");
                        //JAVA 8
                        SunPKCS11 providerPKCS11 = new SunPKCS11("D:\\KNS Digital Signature\\processfile\\cfgfile\\pkcs11.cfg");
                        Security.addProvider(providerPKCS11);
                        KeyStore ks = KeyStore.getInstance("PKCS11");
                        char[] pass = pin.toCharArray();
                        ks.load(null, pass);
                        Enumeration<String> aliases = ks.aliases();
                        List<String> accessCode = new ArrayList<>();
                        log.info(" FileUploadService -> Is Access Codes are available -> " + aliases.hasMoreElements());
                        while (aliases.hasMoreElements()) {
                            accessCode.add(aliases.nextElement());
                        }

                        String dll36Code = null;
                        System.out.println(" access code size " + accessCode.size());
                        if (accessCode.size() == 1) {
                            log.info(" FileUploadService -> Access code-> access code -> " + accessCode.get(0));
                            dll36Code = accessCode.get(0); // bhagya device format -- it have only once access code ..so need to check the format
                        } else {
                            for (String checkFormat : accessCode) {
                                log.info(" FileUploadService -> Access code loop -> access code -> " + checkFormat);

                                boolean isCorrectFormat = checkFormat.matches("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}"); //yasha device format

                                if (isCorrectFormat == true) {
                                    log.info("FileUploadService -> If check format satisfies or matches");
                                    dll36Code = checkFormat;
                                }
                            }
                        }
                        log.info("FileUploadService-> Before response " + providerPKCS11.getName() + " dll 36 code " + dll36Code);
                        res = smartcardsign(providerPKCS11.getName(), ks, dll36Code);
                        log.info("FileUploadService -> Response -> " + res);
                        if (res.equalsIgnoreCase("success")) {
                            // delete the file form source folder location
                            System.out.println(" Inside delete process "+ SRC);
                                    
                            File processedFile = new File(SRC);
                            try {
                                System.gc();
                                processedFile.delete();
                            } catch (Exception e1) {
                                log.info(" FileUploadService -> deleteFileFromSourceFolder() -> Exception -> " + e1.getMessage());
                                e1.printStackTrace();
                            }
                            System.out.println(" Deleted the files successfully");
                            log.info(" FileUploadService -> File Deleted Successfully -> " + SRC);

                        }

                    } catch (IOException e1) {
                        log.info(" FileUploadService ->  IOException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, processFileName + " File is corrupted.", " PDFFile Error",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (DocumentException e1) {
                        log.info(" FileUploadService ->  DocumentException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Document Exception occurred", "DocumentException Exception",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (NoSuchAlgorithmException e1) {
                        log.info(" FileUploadService ->  NoSuchAlgorithmException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Algorithm Exception occurred", "AlgorithmException Exception",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (CertificateException e1) {
                        log.info(" FileUploadService ->  CertificateException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Certificate Exception occurred", "CertificateException Exception",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (KeyStoreException e1) {
                        log.info(" FileUploadService ->  KeyStoreException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Please insert Usb Drive to continue", "KeyStore Error",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (GeneralSecurityException e1) {
                        log.info(" FileUploadService ->  GeneralSecurityException -> " + e1.getMessage());
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "GeneralSecurity Exception occurred", "GeneralSecurity Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } // if 
              }// for
            }// try
                                catch(Exception e){
                                        
                                        }
                
				
				
						
	} // method

	/**
	 * created By Manjunath to get details of provider , keystore, alias
	 * 
	 * @param provider
	 * @param ks
	 * @param alias
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws DocumentException
	 */
	@SuppressWarnings("deprecation")
	public String smartcardsign(String provider, KeyStore ks, String alias)
			throws GeneralSecurityException, IOException, DocumentException {
		log.info("FileUploadService -> Inside smartcardsign service");
		PrivateKey pk = (PrivateKey) ks.getKey(alias, null);
		Certificate[] chain = ks.getCertificateChain(alias);
		OcspClient ocspClient = new OcspClientBouncyCastle();
		//List<CrlClient> crlList = new ArrayList<CrlClient>(); // commented the crlList and passing the crlList as null while adding sign
		//crlList.add(new CrlClientOnline(chain)); // Because CrlList taking more memory for signed file. As per client request, reduced the file size
		String res = signPdf.sign(SRC, String.format(DEST, alias), chain, pk, DigestAlgorithms.SHA256, provider, CryptoStandard.CMS,
				null, null, null, ocspClient, null, 0);
		return res;
	}
	
	public void getPasssword(String[] args) {
		password=args[0];
		
	}
}
