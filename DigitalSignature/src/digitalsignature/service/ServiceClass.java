package digitalsignature.service;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;

import org.apache.log4j.Logger;


import sun.security.pkcs11.SunPKCS11;
/**
 * 
 * @author Manjunath 
 *
 */
public class ServiceClass {
	private static final Logger log= Logger.getLogger(ServiceClass.class);
	char[] pass = null;
	KeyStore ks = null;
	
	public String getLoginUpdate(String password) {
		log.info(" ServiceClass -> getLoginUpdate() ");
		
		try {
                    pass = password.toCharArray();
                    
                    //JAVA 9
                  //  Provider prototype = Security.getProvider("SunPKCS11");
                  //  Provider providerPKCS11 = prototype.configure("D:\\KNS Digital Signature\\processfile\\cfgfile\\pkcs11.cfg");
                  //JAVA 8
		SunPKCS11 providerPKCS11 = new SunPKCS11("D:\\KNS Digital Signature\\processfile\\cfgfile\\pkcs11.cfg");
                log.info(" ServiceClass -> getLoginUpdate() -> after read the provider");
		Security.addProvider(providerPKCS11);
			ks = KeyStore.getInstance("PKCS11", providerPKCS11);
                        log.info("after get the keystore");
			ks.load(null, pass);
                        log.info("success");
			return "Success";
		} catch (KeyStoreException e) {
                    log.info(" ServiceClass -> getLoginUpdate() -> KeyStoreException -> "+e.getMessage());
			e.printStackTrace();
			return "Nousbdrive";
		} catch (NoSuchAlgorithmException e) {
                    log.info(" ServiceClass -> getLoginUpdate() -> NoSuchAlgorithmException -> "+e.getMessage());
			e.printStackTrace();
			return "Algorithm";
		} catch (CertificateException e) {
                    log.info(" ServiceClass -> getLoginUpdate() -> CertificateException -> "+e.getMessage());
			e.printStackTrace();
			return "Certificate";
		} catch (IOException e) {
                    log.info(" ServiceClass -> getLoginUpdate() -> IOException -> "+e.getMessage());
			e.printStackTrace();
			return "Error";
		}
                catch (Exception e) {
                    log.info(" ServiceClass -> getLoginUpdate() -> Exception -> "+e.getMessage());
			e.printStackTrace();
			return "Error";
		}
	}

}
