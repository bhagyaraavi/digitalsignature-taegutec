/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalsignature.controller;
import static digitalsignature.controller.FileUploadScreen.password;
import digitalsignature.fileupload.FileUploadService;
import digitalsignature.service.MonitorDirectory;
import digitalsignature.service.ServiceClass;
import digitalsignature.utility.scheduler.MonitorDirectoryScheduler;
import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author Bhagya
 */
public class DigisignLoginScreen extends javax.swing.JFrame {
private static final Logger log = Logger.getLogger(DigisignLoginScreen.class);
    ServiceClass serviceClass = new ServiceClass();
    String password = null;
    static MonitorDirectoryScheduler monitorDirectoryScheduler=new MonitorDirectoryScheduler();
      
    /**
     * Creates new form DigisignLoginScreen
     */
    public DigisignLoginScreen() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPasswordField2 = new javax.swing.JPasswordField();
        jLabel7 = new javax.swing.JLabel();

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/background.png"))); // NOI18N
        jLabel4.setText("jLabel4");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/background.png"))); // NOI18N
        jLabel5.setText("jLabel5");

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/background.png"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Digital Sign Login");
        setIconImage(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/digi_icon.png")).getImage());
        setPreferredSize(new java.awt.Dimension(560, 350));
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/kns_logo.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(470, 0, 70, 50);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 102, 51));
        jLabel2.setText("Digital Signature Login");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(150, 60, 280, 40);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Enter Password:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(220, 120, 130, 20);

        jButton1.setBackground(new java.awt.Color(102, 51, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Login");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(130, 230, 80, 30);

        jButton2.setBackground(new java.awt.Color(102, 51, 255));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Reset");
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(240, 230, 80, 30);

        jButton3.setBackground(new java.awt.Color(102, 51, 255));
        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText("Exit");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(350, 230, 70, 30);

        jPasswordField2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jPasswordField2);
        jPasswordField2.setBounds(180, 160, 170, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsignature/background.png"))); // NOI18N
        jLabel7.setText("jLabel7");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(0, 0, 598, 350);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        log.info(" DigisignLoginScreen -> Inside Login Button Action Event");
        
        password = jPasswordField2.getText();
        System.out.println(" password "+password);
	String[] args = {password}; 
	String result = serviceClass.getLoginUpdate(password);
        System.out.println(" result from login service "+result);
        if(result.equalsIgnoreCase("success")){
            log.info(" KNSProductsDescScreen -> Login Success");
            FileUploadService fileUploadService =new FileUploadService();
            fileUploadService.getPasssword(args);
					MonitorDirectory.watchDirectoryEvents();
					monitorDirectoryScheduler.schedulerService();
           // FileUploadScreen fileUploadScreen=new FileUploadScreen();
           // fileUploadScreen.main(args);
            //fileUploadScreen.setVisible(true);
             this.dispose();
            
        }
        else if (result.equalsIgnoreCase("nousbdrive")) {
            jPasswordField2.setText(null);
            JOptionPane.showMessageDialog(null, "Please insert Usb Drive to continue", "Login Error", JOptionPane.ERROR_MESSAGE);
        } else if (result.equalsIgnoreCase("Algorithm")) {
            jPasswordField2.setText(null);
            JOptionPane.showMessageDialog(null, "Algorithm Exception occurred", "Login Exception", JOptionPane.ERROR_MESSAGE);
        } else if (result.equalsIgnoreCase("Certificate")) {
            jPasswordField2.setText(null);
            JOptionPane.showMessageDialog(null, "Certificate Exception occurred", "Login Exception", JOptionPane.ERROR_MESSAGE);
        } else {
            jPasswordField2.setText(null);
            JOptionPane.showMessageDialog(null, "Invalid Login Credential", "Login Error", JOptionPane.ERROR_MESSAGE);
        }	
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        log.info(" DigisignLoginScreen -> Inside Exit Button Action Event");
	JFrame exitWindow = new JFrame("Exit");
        if (JOptionPane.showConfirmDialog(exitWindow, "Do you really want to exit!", "Exit",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
            System.exit(0);
        }
			
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        log.info(" DigisignLoginScreen -> Inside Reset Button Action Event");
        jPasswordField2.setText(null);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
       
         
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DigisignLoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DigisignLoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DigisignLoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DigisignLoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DigisignLoginScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPasswordField jPasswordField2;
    // End of variables declaration//GEN-END:variables
}
