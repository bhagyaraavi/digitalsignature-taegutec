package digitalsignature.utility.scheduler;

import java.io.IOException;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import digitalsignature.service.MonitorDirectory;

/**
 * 
 * @author Bhagya
 *  Scheduler job class for to monitor directory
 */
public class MonitorDirectoryJob implements Job
{
	private static final Logger log= Logger.getLogger(MonitorDirectoryJob.class);
	
	/**
	 * Created the Job execution method by bhagya
	 */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info(" MonitorDirectoryJob -> execute() ");
		
			MonitorDirectory.watchDirectoryEvents();
		
		
	}

}